import { useEffect, useState } from 'react';


function AppointmentList(props) {
  const [appointments, setAppointments] = useState([]);
  const loadAppointments = async () => {
    try {
      const response = await fetch('http://localhost:8080/api/appointments/');
      if (response.ok) {
        const data = await response.json();
        setAppointments(data.appointments);
      } else {
        console.error(response);
      }
    } catch (error) {
      console.error(error);
    }
  };
  const handleAction = async (id, action) => {
    const url = `http://localhost:8080/api/appointments/${id}/${action}/`;
    const fetchConfig = { method: 'put' };
    try {
      const response = await fetch(url, fetchConfig);
      if (response.ok) {
        alert(`Successfully ${action === 'cancel' ? 'cancelled' : 'finished'}`);
        loadAppointments();
      } else {
        console.error(response);
      }
    } catch (error) {
      console.error(error);
    }
  };
  useEffect(() => {
    loadAppointments();
  }, []);
  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>VIN</th>
          <th>Is VIP?</th>
          <th>Customer</th>
          <th>Date / Time</th>
          <th>Technician</th>
          <th>Reason</th>
        </tr>
      </thead>
      <tbody>
        {appointments?.map((appointment) => (
          <tr key={appointment.vin}>
            <td>{appointment.vin}</td>
            <td>{appointment.is_vip}</td>
            <td>{appointment.customer}</td>
            <td>{appointment.date_time}</td>
            <td>{appointment.technician.first_name}</td>
            <td>{appointment.reason}</td>
            <td>
              <div>
                <button
                  type="button"
                  className="btn btn-danger"
                  onClick={() => handleAction(appointment.id, 'cancel')}
                >
                  Cancel
                </button>
                <button
                  type="button"
                  className="btn btn-success"
                  onClick={() => handleAction(appointment.id, 'finish')}
                >
                  Finish
                </button>
              </div>
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  );
}
export default AppointmentList;
