import React, { useState } from 'react'


function TechnicianForm () {

    const [first_name, setFirstName] = useState('')
    const [last_name, setLastName] = useState('')
    const [employee_id, setEmployeeId] = useState('')

    const handleFirstNameChange = (event) => {
        const value = event.target.value
        setFirstName(value)
    }

    const handleLastNameChange = (event) => {
        const value = event.target.value
        setLastName(value)
    }

    const handleEmployeeIdChange = (event) => {
        const value = event.target.value
        setEmployeeId(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.first_name = first_name
        data.last_name = last_name
        data.employee_id = employee_id
        const techniciansUrl = `http://localhost:8080/api/technicians/`
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {'Content-Type': 'application/json',}
        }
        const newTechnicianResponse = await fetch(techniciansUrl, fetchConfig)
        if (newTechnicianResponse.ok) {
            setFirstName('')
            setLastName('')
            setEmployeeId('')
            alert("Technician successfully added!")
        } else {
            console.error('error:', newTechnicianResponse.statusText, newTechnicianResponse.status, "employee id taken, try different employee id")
            alert('employee id taken, use different employee id')
        }
    }

    return (
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div id="formShadow" style={{backgroundColor: "rgba(210, 238, 130, .5)", boxShadow: '0px 0px 25px 15px rgb(232, 255, 195)'}} className="p-4 mt-4">
                        <h1 style={{fontFamily: 'orbitron'}}>Add a Technician</h1>
                        <form onSubmit={handleSubmit} id="create-technician-form">
                            <label style={{fontFamily: 'orbitron'}} htmlFor="first_name">First name...</label>
                            <div className="form-floating mb-3">
                                <input onChange={handleFirstNameChange} value={first_name} placeholder="first_name" required type="text" name="first_name" id="first_name" className="form-control"/>
                                <label style={{color: 'rgba(171, 183, 183, 1)'}} htmlFor="first_name">First name...</label>
                            </div>
                            <label style={{fontFamily: 'orbitron'}} htmlFor="last_name">Last name...</label>
                            <div className="form-floating mb-3">
                                <input onChange={handleLastNameChange} value={last_name} placeholder="last_name" required type="text" name="last_name" id="last_name" className="form-control"/>
                                <label style={{color: 'rgba(171, 183, 183, 1)'}} htmlFor="last_name">Last name...</label>
                            </div>
                            <label style={{fontFamily: 'orbitron'}} htmlFor="style">Employee ID...</label>
                            <div className="form-floating mb-3">
                                <input onChange={handleEmployeeIdChange} value={employee_id} placeholder="employee_id" required type="text" name="employee_id" id="employee_id" className="form-control"/>
                                <label style={{color: 'rgba(171, 183, 183, 1)'}} htmlFor="style">Employee ID...</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default TechnicianForm
