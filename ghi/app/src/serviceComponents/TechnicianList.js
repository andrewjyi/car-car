import React, {useState, useEffect} from 'react'


function TechniciansList() {

    const [technicians, setTechnicians] = useState([])

    const getTechniciansData = async () => {
        const response = await fetch('http://localhost:8080/api/technicians/')
        if (response.ok) {
            const techniciansData = await response.json()
            setTechnicians(techniciansData.technicians)
        } else {
            console.error("An error occurred while fetching the data")
        }
    }

    useEffect(() => {
        getTechniciansData()
    }, [])

    return (
        <div style={{backgroundColor: "rgba(210, 238, 130, .9)", paddingLeft: '30px', paddingRight: '30px', boxShadow: '0px 0px 25px 15px rgb(232, 255, 195)'}} className="container">
            <div className="my-5 container">
                <div className="row">
                    <h1 style={{fontFamily: 'orbitron', paddingTop: '20px', paddingBottom: '20px'}}>Technicians</h1>
                    <table className="table table-striped table-hover">
                        <thead style={{fontFamily: 'orbitron'}}>
                            <tr>
                                <th>Employee ID</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                            </tr>
                        </thead>
                        <tbody>
                            {technicians.map(technician => {
                                return (
                                    <tr key={ technician.href }>
                                        <td>{ technician.employee_id }</td>
                                        <td>{ technician.first_name }</td>
                                        <td>{ technician.last_name }</td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    )
}

export default TechniciansList
