import React, {useState, useEffect} from 'react'


function AutomobilesList() {

    const [automobiles, setAutomobiles] = useState([])

    const [filter, setFilter] = useState('')

    const getAutomobilesData = async () => {
        const response = await fetch('http://localhost:8100/api/automobiles/')
        if (response.ok) {
            const automobilesData = await response.json()
            setAutomobiles(automobilesData.autos)
        } else {
            console.error("An error occurred while fetching the data", response.statusText, response.status)
        }
    }

    useEffect(() => {
        getAutomobilesData()
    }, [])

    const handleFilterChange = (event) => {
        setFilter(event.target.value);
    }

    const filterAutomobiles = (numString) => {
        if (numString === "1") {
            return automobiles.filter(automobile => automobile.sold === false)
        } else if (numString === "2") {
            return automobiles.filter(automobile => automobile.sold === true)
        } else {
            return automobiles
        }
    }

    const filteredAutomobiles = filterAutomobiles(filter)

    let dropdownClasses = 'form-select'

    return (
        <div style={{backgroundColor: "rgba(210, 238, 130, .9)", width: 'fit-content', paddingLeft: '30px', paddingRight: '30px', boxShadow: '0px 0px 25px 15px rgb(232, 255, 195)'}} className="container">
            <div className="my-5 container">
                <div className="row">
                    <h1 style={{fontFamily: 'orbitron', paddingTop: '20px'}}>Automobiles</h1>
                    <div className="mb-3">
                            <select onChange={handleFilterChange} style={filter.length>0?{fontFamily: 'orbitron'}:{color: 'rgba(171, 183, 183, 1)'}} required name="salesperson" id="salesperson" className={dropdownClasses}>
                                {filter.length === 0?<option value="">Choose filter</option>:<option style={{fontFamily: 'orbitron', color: '#212529'}} value="">Click to view all</option>}
                                <option style={{fontFamily: 'orbitron', color: '#212529'}} value="1">Click to view available</option>
                                <option style={{fontFamily: 'orbitron', color: '#212529'}} value="2">Click to view sold</option>
                            </select>
                        </div>
                    <table className="table table-striped table-hover">
                        <thead style={{fontFamily: 'orbitron'}}>
                            <tr>
                                <th>VIN</th>
                                <th>Color</th>
                                <th>Year</th>
                                <th>Model</th>
                                <th>Manufacturer</th>
                                <th>Sold</th>
                            </tr>
                        </thead>
                        <tbody>
                            {filteredAutomobiles.map(automobile => {
                                return (
                                    <tr key={ automobile.href }>
                                        <td>{ automobile.vin }</td>
                                        <td>{ automobile.color }</td>
                                        <td>{ automobile.year }</td>
                                        <td>{ automobile.model.name }</td>
                                        <td>{ automobile.model.manufacturer.name }</td>
                                        <td>{ automobile.sold?'Yes':'No' }</td>
                                    </tr>
                                );
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    )
}

export default AutomobilesList
