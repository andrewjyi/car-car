import React, { useEffect, useState } from 'react'


function AutomobileForm () {

    const [models, setModels] = useState([])

    const [color, setColor] = useState('')
    const [year, setYear] = useState('')
    const [vin, setVin] = useState('')
    const [model, setModel] = useState('')

    const handleColorChange = (event) => {
        const value = event.target.value
        setColor(value)
    }
    const handleYearChange = (event) => {
        const value = event.target.value
        setYear(value)
    }
    const handleVinChange = (event) => {
        const value = event.target.value
        setVin(value)
    }
    const handleModelChange = (event) => {
        const value = event.target.value
        setModel(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.color = color
        data.year = year
        data.vin = vin
        data.model_id = model
        const automobilesUrl = `http://localhost:8100/api/automobiles/`
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {'Content-Type': 'application/json',}
        }
        const newAutomobileResponse = await fetch(automobilesUrl, fetchConfig)
        if (newAutomobileResponse.ok) {
            setColor('')
            setYear('')
            setVin('')
            setModel('')
            alert("Automobile successfully added!")
        } else {
            console.error('error:', newAutomobileResponse.statusText, newAutomobileResponse.status, "most likely VIN error, please verify and correct!")
            alert('Please verify and correct the VIN!')
        }
    }

    const fetchData = async () => {
        const modelsUrl = `http://localhost:8100/api/models/`
        const response = await fetch(modelsUrl)

        if (response.ok) {
            const modelsData = await response.json()
            setModels(modelsData.models)
        }
    }

    useEffect(() =>{
        fetchData()
    }, [])

    // let dropdownClasses = 'form-select d-none'
    // if (models.length > 0) {
    const dropdownClasses = 'form-select'
    // }

    return (
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div style={{backgroundColor: "rgba(210, 238, 130, .5)", boxShadow: '0px 0px 25px 15px rgb(232, 255, 195)'}} className="p-4 mt-4">
                        <h1 style={{fontFamily: 'orbitron'}}>Add Inventory Automobile</h1>
                        <form onSubmit={handleSubmit} id="create-automobile-form">
                            <label style={{fontFamily: 'orbitron'}} htmlFor="color">Color</label>
                            <div className="form-floating mb-3">
                                <input onChange={handleColorChange} value={color} placeholder="color" required type="text" name="color" id="color" className="form-control"/>
                                <label style={{color: 'rgba(171, 183, 183, 1)'}} htmlFor="color">Color...</label>
                            </div>
                            <label style={{fontFamily: 'orbitron'}} htmlFor="year">Year</label>
                            <div className="form-floating mb-3">
                                <input onChange={handleYearChange} value={year} placeholder="year" required type="number" name="year" id="year" className="form-control"/>
                                <label style={{color: 'rgba(171, 183, 183, 1)'}} htmlFor="year">Year...</label>
                            </div>
                            <label style={{fontFamily: 'orbitron'}} htmlFor="vin">VIN</label>
                            <div className="form-floating mb-3">
                                <input onChange={handleVinChange} value={vin} placeholder="vin" required type="text" name="vin" id="vin" className="form-control"/>
                                <label style={{color: 'rgba(171, 183, 183, 1)'}} htmlFor="vin">VIN...</label>
                            </div>
                            <label style={{fontFamily: 'orbitron'}} htmlFor="vin">Vehicle Model</label>
                            <div className="mb-3">
                                <select onChange={handleModelChange} value={model} style={model.length>0?{fontFamily: 'cinzel decorative', fontWeight: '400'}:{color: 'rgba(171, 183, 183, 1)'}} required name="model" id="model" className={dropdownClasses}>
                                    <option style={{color: 'rgba(171, 183, 183, 1)'}} value="">Choose a model...</option>
                                    {models.map(model => {
                                        return (
                                            <option style={{fontFamily: 'cinzel decorative', fontWeight: '400', color: '#212529'}} key={model.href} value={model.id}>{model.manufacturer.name} {model.name}</option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default AutomobileForm
