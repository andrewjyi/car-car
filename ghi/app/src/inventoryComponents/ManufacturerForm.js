import React, { useState } from 'react'


function ManufacturerForm () {

    const [name, setName] = useState('')
    const [pictureUrl, setPictureUrl] = useState('')

    const handleNameChange = (event) => {
        const value = event.target.value
        setName(value)
    }

    const handlePictureUrlChange = (event) => {
        const value = event.target.value
        setPictureUrl(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.name = name
        data.picture_url = pictureUrl

        const manufacturersUrl = `http://localhost:8100/api/manufacturers/`
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {'Content-Type': 'application/json',}
        }
        const newManufacturerResponse = await fetch(manufacturersUrl, fetchConfig)
        if (newManufacturerResponse.ok) {
            setName('')
            setPictureUrl('')
            alert("Manufacturer successfully added!")
        } else {
            console.error('error:', newManufacturerResponse.statusText, newManufacturerResponse.status)
        }
    }

    return (
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div style={{backgroundColor: "rgba(210, 238, 130, .5)", boxShadow: '0px 0px 25px 15px rgb(232, 255, 195)'}} className="p-4 mt-4">
                        <h1 style={{fontFamily: 'orbitron'}}>Create a Manufacturer</h1>
                        <form onSubmit={handleSubmit} id="create-manufacturer-form">
                            <label style={{fontFamily: 'orbitron'}} htmlFor="name">Manufacturer</label>
                            <div className="form-floating mb-3">
                                <input onChange={handleNameChange} value={name} placeholder="name" required type="text" name="name" id="name" className="form-control"/>
                                <label style={{color: 'rgba(171, 183, 183, 1)'}} htmlFor="name">Name...</label>
                            </div>
                            <label style={{fontFamily: 'orbitron'}} htmlFor="picture_url">Picture Url</label>
                            <div className="form-floating mb-3">
                                <input onChange={handlePictureUrlChange} value={pictureUrl} placeholder="picture_url" type="url" name="picture_url" id="picture_url" className="form-control"/>
                                <label style={{color: 'rgba(171, 183, 183, 1)'}} htmlFor="picture_url">Leave blank: autogenerate...</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ManufacturerForm
