import React, {useState, useEffect} from 'react';


function ManufacturersList() {

    const [manufacturers, setManufacturers] = useState([]);

    const getManufacturersData = async () => {
        const response = await fetch('http://localhost:8100/api/manufacturers/');
        if (response.ok) {
            const manufacturersData = await response.json();
            setManufacturers(manufacturersData.manufacturers);
        } else {
            console.error("An error occurred while fetching the data", response.statusText, response.status);
        }
    }

    useEffect(() => {
        getManufacturersData();
    }, []);

    return (
        <div style={{backgroundColor: "rgba(210, 238, 130, .9)", width: 'fit-content', paddingLeft: '30px', paddingRight: '30px', boxShadow: '0px 0px 25px 15px rgb(232, 255, 195)'}} className="container">
            <div className="my-5 container">
                <div className="row">
                    <h1 style={{fontFamily: 'orbitron', paddingTop: '20px'}}>Manufacturers</h1>
                    <table className="table table-striped table-hover">
                        <thead style={{boxSizing:'content-box', height:'80px', fontFamily: 'orbitron'}}>
                            <tr>
                                <th>Name</th>
                                <th>Picture</th>
                            </tr>
                        </thead>
                        <tbody style={{fontFamily: 'cinzel decorative', fontWeight: 400, fontSize: 'large'}}>
                            {manufacturers.map(manufacturer => {
                                return (
                                    <tr key={ manufacturer.href }>
                                        <td>{ manufacturer.name }</td>
                                        <td><img style={{width: "150px", height: "100px"}} alt={`Manufacturer ${manufacturer.name}`} src={ manufacturer.picture_url }/></td>
                                    </tr>
                                );
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    )
}

export default ManufacturersList;
