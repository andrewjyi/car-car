import React, {useState, useEffect} from 'react'


function ModelsList() {

    const [models, setModels] = useState([])

    const getModelsData = async () => {
        const response = await fetch('http://localhost:8100/api/models/')
        if (response.ok) {
            const modelsData = await response.json()
            setModels(modelsData.models)
        } else {
            console.error("An error occurred while fetching the data", response.statusText, response.status)
        }
    }

    useEffect(() => {
        getModelsData()
    }, [])

    return (
        <div style={{backgroundColor: "rgba(210, 238, 130, .9)", width: 'fit-content', paddingLeft: '30px', paddingRight: '30px', boxShadow: '0px 0px 25px 15px rgb(232, 255, 195)'}} className="container">
            <div className="my-5 container">
                <div className="row">
                    <h1 style={{fontFamily: 'orbitron', paddingTop: '20px'}}>Models</h1>
                    <table className="table table-striped table-hover">
                        <thead style={{boxSizing:'content-box', height:'80px', fontFamily: 'orbitron'}}>
                            <tr>
                                <th>Name</th>
                                <th>Manufacturer</th>
                                <th>Picture</th>
                            </tr>
                        </thead>
                        <tbody style={{fontFamily: 'cinzel decorative'}}>
                            {models.map(model => {
                                return (
                                    <tr key={ model.href }>
                                        <td>{ model.name }</td>
                                        <td>{ model.manufacturer.name }</td>
                                        <td><img style={{width: "150px", height: "100px"}} alt={`${model.manufacturer.name} ${model.name} model`} src={ model.picture_url }/></td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    )
}

export default ModelsList
