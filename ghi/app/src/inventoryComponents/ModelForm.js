import React, { useEffect, useState } from 'react'


function ModelForm () {

    const [manufacturers, setManufacturers] = useState([])

    const [name, setName] = useState('')
    const [picture_url, setPictureUrl] = useState('')
    const [manufacturer, setManufacturer] = useState('')

    const handleNameChange = (event) => {
        const value = event.target.value
        setName(value)
    }

    const handlePictureUrlChange = (event) => {
        const value = event.target.value
        setPictureUrl(value)
    }

    const handleManufacturerChange = (event) => {
        const value = event.target.value
        setManufacturer(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.name = name
        data.picture_url = picture_url
        data.manufacturer_id = manufacturer
        const modelsUrl = `http://localhost:8100/api/models/`
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {'Content-Type': 'application/json',}
        }
        const newModelResponse = await fetch(modelsUrl, fetchConfig)
        if (newModelResponse.ok) {
            setName('')
            setPictureUrl('')
            setManufacturer('')
            alert("Manufacturer successfully added!")
        } else {
            console.error('error:', newModelResponse.statusText, newModelResponse.status, "most likely an error with the picture_url, please verify and resubmit!")
            alert('Please verify and correct the picture URL')
        }
    }

    const fetchData = async () => {
        const manufacturersUrl = `http://localhost:8100/api/manufacturers/`
        const response = await fetch(manufacturersUrl)
        if (response.ok) {
            const manufacturersData = await response.json()
            setManufacturers(manufacturersData.manufacturers)
        }
    }

    useEffect(() =>{
        fetchData()
    }, [])

    // let dropdownClasses = 'form-select d-none'
    // if (manufacturers.length > 0) {
    const dropdownClasses = 'form-select'
    // }

    return (
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div style={{backgroundColor: "rgba(210, 238, 130, .5)", boxShadow: '0px 0px 25px 15px rgb(232, 255, 195)'}} className="p-4 mt-4">
                        <h1 style={{fontFamily: 'orbitron'}}>Create a Vehicle Model</h1>
                        <form onSubmit={handleSubmit} id="create-model-form">
                            <label style={{fontFamily: 'orbitron'}} htmlFor="name">Model</label>
                            <div className="form-floating mb-3">
                                <input onChange={handleNameChange} value={name} placeholder="name" required type="text" name="name" id="name" className="form-control"/>
                                <label style={{color: 'rgba(171, 183, 183, 1)'}} htmlFor="name">Name...</label>
                            </div>
                            <label style={{fontFamily: 'orbitron'}} htmlFor="picture_url">Picture Url</label>
                            <div className="form-floating mb-3">
                                <input onChange={handlePictureUrlChange} value={picture_url} placeholder="picture_url" type="url" name="picture_url" id="picture_url" className="form-control"/>
                                <label style={{color: 'rgba(171, 183, 183, 1)'}} htmlFor="picture_url">Leave blank: autogenerate</label>
                            </div>
                            <label style={{fontFamily: 'orbitron'}} htmlFor="manufacturer">Manufacturer</label>
                            <div className="mb-3">
                                <select onChange={handleManufacturerChange} value={manufacturer} style={manufacturer.length>0?{fontFamily: 'cinzel decorative', fontWeight: '400'}:{color: 'rgba(171, 183, 183, 1)'}} required name="manufacturer" id="manufacturer" className={dropdownClasses}>
                                    <option style={{color: 'rgba(171, 183, 183, 1)'}} value="">Choose a manufacturer...</option>
                                    {manufacturers.map(manufacturer => {
                                        return (
                                            <option style={{fontFamily: 'cinzel decorative', fontWeight: '400', color: '#212529'}} key={manufacturer.href} value={manufacturer.id}>{manufacturer.name}</option>
                                        )
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ModelForm
