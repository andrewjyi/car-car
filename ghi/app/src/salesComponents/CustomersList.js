import React, { useEffect, useState } from 'react';


function CustomersList() {

    const [customers, setCustomers] = useState([]);

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/customers/');
        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers);
        } else {
            console.error('An error occurred fetching the data');
        }
    };

    useEffect(() => {
        getData();
    }, []);

    return (
        <div style={{backgroundColor: "rgba(210, 238, 130, .9)", width: 'fit-content', paddingLeft: '30px', paddingRight: '30px', boxShadow: '0px 0px 25px 15px rgb(232, 255, 195)'}} className="container">
            <div className="my-5 container">
                <div className="row">
                    <h1 style={{fontFamily: 'orbitron', paddingTop: '20px', paddingBottom: '20px'}}>Customer List</h1>
                    <table className="table table-striped table-hover">
                        <thead style={{boxSizing:'content-box', fontFamily: 'orbitron'}}>
                            <tr>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Address</th>
                                <th>Phone Number</th>
                            </tr>
                        </thead>
                        <tbody>
                            {customers?.map(customer => {
                                return (
                                    <tr key={ customer.href } value={ customer.id }>
                                        <td>{ customer.first_name }</td>
                                        <td>{ customer.last_name }</td>
                                        <td>{ customer.address }</td>
                                        <td>{ customer.phone_number }</td>
                                    </tr>
                                );
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
}

export default CustomersList;
