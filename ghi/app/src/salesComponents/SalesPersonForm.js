import React, { useState } from 'react';


function SalesPersonForm() {

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [employeeID, setEmployeeId] = useState('');


    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
            data.first_name = firstName;
            data.last_name = lastName;
            data.employee_id = employeeID;
        const salesUrl = 'http://localhost:8090/api/salespeople/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(salesUrl, fetchConfig);
        if (response.ok) {
            setFirstName('');
            setLastName('');
            setEmployeeId('');
            alert('salesperson successfully added');
        } else {
            console.error('error:', response.statusText, response.status, "employee id taken, try different employee id");
            alert('employee id taken, use different employee id');
        }
    }

    const handleFirstNameChange = (event) => {
        const value = event.target.value;
        setFirstName(value);
    }

    const handleLastNameChange = (event) => {
        const value = event.target.value;
        setLastName(value);
    }

    const handleEmployeeIdChange = (event) => {
        const value = event.target.value;
        setEmployeeId(value);
    }

    return (
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div style={{backgroundColor: "rgba(210, 238, 130, .5)", boxShadow: '0px 0px 25px 15px rgb(232, 255, 195)'}} className="p-4 mt-4">
                        <h1 style={{fontFamily: 'orbitron'}}>Add Sales Personnel</h1>
                        <form onSubmit={handleSubmit} id="create-salesperson-form">
                            <label style={{fontFamily: 'orbitron'}} htmlFor="name">First Name</label>
                            <div className="form-floating mb-3">
                                <input value={firstName} onChange={handleFirstNameChange} placeholder="Name" required type="text" name="first_name" id="name" className="form-control" />
                                <label style={{color: 'rgba(171, 183, 183, 1)'}} htmlFor="name">First Name...</label>
                            </div>
                            <label style={{fontFamily: 'orbitron'}} htmlFor="size">Last Name</label>
                            <div className="form-floating mb-3">
                                <input value={lastName} onChange={handleLastNameChange} placeholder="last_name" required type="text" name="last_name" id="" className="form-control" />
                                <label style={{color: 'rgba(171, 183, 183, 1)'}} htmlFor="size">Last Name...</label>
                            </div>
                            <label style={{fontFamily: 'orbitron'}} htmlFor="color">Employee ID</label>
                            <div className="form-floating mb-3">
                                <input value={employeeID} onChange={handleEmployeeIdChange} placeholder="employeeID" required type="number" name="employeeID" id="" className="form-control" />
                                <label style={{color: 'rgba(171, 183, 183, 1)'}} htmlFor="color">Employee ID...</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default SalesPersonForm;
