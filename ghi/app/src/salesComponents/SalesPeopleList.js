import React, { useEffect, useState } from 'react';


function SalesPeopleList() {

    const [salespersons, setSalesPersons] = useState([]);

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/salespeople/');
        if (response.ok) {
            const data = await response.json();
            setSalesPersons(data.salespersons);
        } else {
            console.error('An error occurred fetching the data');
        }
    };

    useEffect(() => {
        getData();
    }, []);

    return (
        <div style={{backgroundColor: "rgba(210, 238, 130, .9)", width: 'fit-content', paddingLeft: '30px', paddingRight: '30px', boxShadow: '0px 0px 25px 15px rgb(232, 255, 195)'}} className="container">
            <div className="my-5 container">
                <div className="row">
                    <h1 style={{fontFamily: 'orbitron', paddingTop: '20px', paddingBottom: '20px'}}>Sales Personnel</h1>
                    <table className="table table-striped table-hover">
                        <thead style={{boxSizing:'content-box', fontFamily: 'orbitron'}}>
                            <tr>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Employee ID</th>
                            </tr>
                        </thead>
                        <tbody>
                            {salespersons?.map(salesperson => {
                                return (
                                <tr key={salesperson.id} value={salespersons.id}>
                                    <td>{salesperson.first_name}</td>
                                    <td>{salesperson.last_name}</td>
                                    <td>{salesperson.employee_id}</td>
                                </tr>
                            )})}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
}

export default SalesPeopleList;
