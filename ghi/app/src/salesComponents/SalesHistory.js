import React, { useEffect, useState } from 'react';


function SalesList() {

    const [sales, setSales] = useState([]);

    const [salesPeople, setSalesPeople] = useState([]);

    const [employeeId, setEmployeeId] = useState('');
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/sales/');
        const salesPeopleUrl = `http://localhost:8090/api/salespeople/`;
        const salesPeopleResponse = await fetch(salesPeopleUrl);
        if (response.ok) {
            const salesData = await response.json();
            setSales(salesData.sale);
        } else {
            console.error('An error occurred fetching the data');
        }
        if (salesPeopleResponse.ok) {
            const salesPeopleData = await salesPeopleResponse.json();
            setSalesPeople(salesPeopleData.salespersons);
        } else {
            console.error('An error occurred while fetching the salespeople data:', salesPeopleResponse.statusText, salesPeopleResponse.status);
        }
    };

    useEffect(() => {
        getData();
    }, []);

    const handleSalesPersonChange = (event) => {
        setEmployeeId(event.target.value.split(',')[0]);
        setFirstName(event.target.value.split(',')[1]);
        setLastName(event.target.value.split(',')[2]);
    }

    const filterByName = (first, last, employeeId) => {
        return sales.filter(sale => sale.sales_person.first_name === first).filter(sale => sale.sales_person.last_name === last).filter(sale => sale.sales_person.employee_id.toString() === employeeId);
    }

    const filteredSales = filterByName(firstName, lastName, employeeId);

    // let dropdownClasses = 'form-select d-none';
    // if (salesPeople.length > 0) {
    const dropdownClasses = 'form-select';
    // }

    return (
        <div style={{backgroundColor: "rgba(210, 238, 130, .9)", minWidth: 'fit-content', paddingLeft: '30px', paddingRight: '30px', boxShadow: '0px 0px 25px 15px rgb(232, 255, 195)'}} className="container">
            <div className="my-5 container">
                <div className="row">
                    <h1 style={{fontFamily: 'orbitron', paddingTop: '20px'}}>Salesperson Sales History</h1>
                        <div className="mb-3">
                            <select onChange={handleSalesPersonChange} style={employeeId.length>0?{fontFamily: 'orbitron'}:{color: 'rgba(171, 183, 183, 1)'}} required name="salesperson" id="salesperson" className={dropdownClasses}>
                                {employeeId.length === 0?<option style={{color: 'rgba(171, 183, 183, 1)'}} value="">Choose a salesperson...</option>:<option style={{fontFamily: 'orbitron', color: '#212529'}} value="">Click to view all</option>}
                                {salesPeople.map(salesperson => {
                                    return (
                                        <option style={{fontFamily: 'orbitron', color: '#212529'}} key={salesperson.href} value={[salesperson.employee_id, salesperson.first_name, salesperson.last_name]}>Employee ID: {salesperson.employee_id} &nbsp;&nbsp; Name: {salesperson.first_name} {salesperson.last_name}</option>
                                    );
                                })}
                            </select>
                        </div>
                    <table className="table table-striped table-hover">
                        <thead style={{fontFamily: 'orbitron'}}>
                            <tr>
                                <th>Employee Name</th>
                                <th>Employee ID</th>
                                <th>Customer</th>
                                <th>Price</th>
                                <th>Vin</th>
                            </tr>
                        </thead>
                        <tbody>
                            { employeeId.length>0?filteredSales.map(sale => {
                                return (
                                    <tr key={sale.id} value={sale.id}>
                                        <td>{sale.sales_person.first_name}</td>
                                        <td>{sale.sales_person.employee_id}</td>
                                        <td>{sale.customer.first_name}</td>
                                        <td>{sale.price}</td>
                                        <td>{sale.automobile.vin}</td>
                                    </tr>
                                )}):sales.map(sale => {
                                return (
                                    <tr key={sale.href} value={sale.id}>
                                        <td>{sale.sales_person.first_name}</td>
                                        <td>{sale.sales_person.employee_id}</td>
                                        <td>{sale.customer.first_name}</td>
                                        <td>{sale.price}</td>
                                        <td>{sale.automobile.vin}</td>
                                    </tr>
                                )})
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
}

export default SalesList;
