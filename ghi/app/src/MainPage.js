import dragon from './dragon.jpg'
import greenDragon from './greenDragon.png'
import { useAuth } from './reactUserAuthComponents/AuthProvider'
import { useNavigate } from 'react-router-dom'


function MainPage() {

  const auth = useAuth()
  const navigate = useNavigate()

  return (
    <div style={{backgroundColor: "rgba(210, 238, 130, .5)"}} className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">CarCar</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          <b>
            The premiere solution for automobile dealership
            management!
          </b>
        </p>
        {!auth.token?(<><button id="login" onClick={() => navigate('/login/')} className="btn btn-success"><img style={{height: "50px", width: "70px", boxShadow: '0px 0px 25px 15px rgb(232, 255, 195)'}} src={dragon} alt="Dragon Emblem"/></button><p><b>Sign in!</b></p></>):(<><button id="logout" onClick={() => auth.logOut()} className="btn btn-success"><img style={{height: "50px", width: "70px", boxShadow: '0px 0px 25px 15px rgb(232, 255, 195)'}} src={greenDragon} alt="Green Dragon Emblem"/></button><p><b>Sign out!</b></p></>)}
      </div>
    </div>
  );
}

export default MainPage;
