import greenDragon from './greenDragon.png'


function MusicVideo() {

    return (
        <>
            <div style={{backgroundColor: "rgba(210, 238, 130, .9)", marginTop: '60px', width: 'fit-content', paddingLeft: '5px', paddingRight: '5px', boxShadow: '0px 0px 25px 15px rgb(232, 255, 195)'}} className="container">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/Mc2-YM9Bhu4?si=cuTuXbsOmZrLLzRS&vq=hd1080&showinfo=0&rel=0&autoplay=1&mute=1&loop=1&playlist=Mc2-YM9Bhu4" title="YouTube video player" allow="accelerometer; autoplay; loop; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"></iframe>
            </div>
        </>
    )
}

export default MusicVideo
