import { BrowserRouter, Routes, Route } from 'react-router-dom'
import MainPage from './MainPage'
import Nav from './Nav'
import CustomerForm from './salesComponents/CustomerForm'
import CustomersList from './salesComponents/CustomersList'
import SalesList from './salesComponents/SalesList'
import SalesForm from './salesComponents/SalesForm'
import SalesPersonForm from './salesComponents/SalesPersonForm'
import SalesPeopleList from './salesComponents/SalesPeopleList'
import SalesHistory from './salesComponents/SalesHistory'
import TechnicianForm from './serviceComponents/TechnicianForm'
import TechniciansList from './serviceComponents/TechnicianList'
import ServiceForm from './serviceComponents/ServiceForm'
import ServicesList from './serviceComponents/ServiceList'
import ServiceHistory from './serviceComponents/ServiceHistory'
import ManufacturersList from './inventoryComponents/ManufacturersList'
import ManufacturerForm from './inventoryComponents/ManufacturerForm'
import ModelsList from './inventoryComponents/ModelsList'
import ModelForm from './inventoryComponents/ModelForm'
import AutomobilesList from './inventoryComponents/AutomobilesList'
import AutomobileForm from './inventoryComponents/AutomobileForm'
import MusicVideo from './musicVideoComponents/MusicVideo'

import AuthProvider from './reactUserAuthComponents/AuthProvider'
import LoginForm from './reactUserAuthComponents/LoginForm'
import LoggedIn from './reactUserAuthComponents/LoggedIn'
import LogoutForm from './reactUserAuthComponents/LogoutForm'
import SignupForm from './reactUserAuthComponents/SignupForm'


function App() {

  return (
    <BrowserRouter>
      <AuthProvider>
      <Nav />
        <Routes>

          <Route path="/" element={<MainPage />} />

          <Route path="/login/" element={<LoginForm />} />
          <Route path="/logout/" element={<LogoutForm />} />
          <Route path="/signup/" element={<SignupForm />} />

          <Route element={<LoggedIn />}>

            <Route path="/manufacturers/" element={<ManufacturersList />} />
            <Route path="/manufacturers/new/" element={<ManufacturerForm />} />
            <Route path="/models/" element={<ModelsList />} />
            <Route path="/models/new/" element={<ModelForm />} />
            <Route path="/automobiles/" element={<AutomobilesList />} />
            <Route path="/automobiles/new/" element={<AutomobileForm />} />

            <Route path="/salespeople/" element={<SalesPeopleList />} />
            <Route path="/salespeople/new/" element={<SalesPersonForm />} />
            <Route path="/customers/" element={<CustomersList />} />
            <Route path="/customers/new/" element={<CustomerForm />} />
            <Route path="/sales/" element={<SalesList />} />
            <Route path="/sales/new/" element={<SalesForm />} />
            <Route path="/sales/history/" element={<SalesHistory />} />

            <Route path="/technicians/" element={<TechniciansList />} />
            <Route path="/technicians/new/" element={<TechnicianForm />} />
            <Route path="/services/" element={<ServicesList />} />
            <Route path="/services/new/" element={<ServiceForm />} />
            <Route path="/services/history/" element={<ServiceHistory />} />

          </Route>

        </Routes>

        <MusicVideo />

      </AuthProvider>
    </BrowserRouter>
  )
}

export default App
