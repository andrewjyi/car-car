import { useContext, createContext, useState } from "react"
import { useNavigate } from "react-router-dom"


const AuthContext = createContext()

function AuthProvider({ children }) {

    const [user, setUser] = useState(null)
    const [token, setToken] = useState(localStorage.getItem("site") || "")

    const navigate = useNavigate()

    const signupAction = async (formData) => {
        const fetchOptions = {
            method: "POST",
            body:JSON.stringify(formData),
            headers: {"Content-type": "application/json"},
        }
        try {
            const response =await fetch("http://localhost:8070/api/signup/", fetchOptions)
            if (response.ok) {
                alert("Successful signup! Please login!")
                navigate("/login/")
                return
            } else {
                console.error('error:', response.statusText, response.status)
                alert('Please verify inputs')
            }
        } catch (err) {
            console.error(err)
        }
    }

    const loginAction = async (formData) => {
        const fetchOptions = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {"Content-type": "application/json"},
        }
        try {
            const response = await fetch("http://localhost:8070/api/login/", fetchOptions)
            const userData = await response.json()
            if (userData.user) {    //backend return JsonResponse({"user": {userData, "token":token} })
                setUser(userData.user)
                setToken(userData.user.token)
                localStorage.setItem("site", userData.token)
                navigate("/")  //homepage
                alert("Successful login!")
                return
            }
            throw new Error(userData.message)
        } catch (err) {
            console.error(err)
        }
    }

    const logOut = () => {
        setUser(null)
        setToken("")
        localStorage.removeItem("site")
        alert("Successful logout!")
        navigate("/") //homepage
    }

    return (
        <AuthContext.Provider value={{ token, user, loginAction, logOut, signupAction }}>
            {children}
        </AuthContext.Provider>
    )
}

export default AuthProvider

export const useAuth = () => {
    return useContext(AuthContext)
}
