import { useState } from 'react'
import { useAuth } from './AuthProvider'


function SignupForm() {

    const [formData, setFormData] = useState( { username: '', first_name: '', last_name: '', email: '', password: '', password_confirmation: '' } )

    const auth = useAuth()

    const handleSubmit = (event) => {
        event.preventDefault()
        if (formData.username !== '' && formData.first_name !== '' && formData.last_name !== '' && formData.email !== '' && formData.password !== '' && formData.password_confirmation !== '' && formData.password === formData.password_confirmation) {
            auth.signupAction(formData)       //useAuth() to use signupAction
            return
        }
        alert("please provide valid inputs")
    }

    const handleInputChange = (event) => {
        const { name, value } = event.target  // name = event.target.name    value = event.target.value
        setFormData((formData) => ({
            ...formData,
            [name]: value,             // will set form data with firstName, lastName, email and password inputs as they type it
        }))
    }

    return (
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div style={{backgroundColor: "rgba(210, 238, 130, .5)", boxShadow: '0px 0px 25px 15px rgb(232, 255, 195)'}} className="p-4 mt-4">
                        <h1 style={{fontFamily: 'orbitron'}}>Create Account</h1>
                        <form onSubmit={handleSubmit}>
                            <label style={{fontFamily: 'orbitron'}} htmlFor="username">Username:</label>
                            <div className="form-floating mb-3">
                                <input onChange={handleInputChange} required className="form-control" id="username" type="text" name="username" placeholder="username" aria-describedby="username" aria-invalid="false" />
                                <label style={{color: 'rgba(171, 183, 183, 1)'}} htmlFor="username">username...</label>
                            </div>
                            <label style={{fontFamily: 'orbitron'}} htmlFor="firstName">First Name:</label>
                            <div className="form-floating mb-3">
                                <input onChange={handleInputChange} required className="form-control" id="firstName" type="text" name="first_name" placeholder="firstName" aria-describedby="firstName" aria-invalid="false" />
                                <label style={{color: 'rgba(171, 183, 183, 1)'}} htmlFor="firstName">first name...</label>
                            </div>
                            <label style={{fontFamily: 'orbitron'}} htmlFor="lastName">Last Name:</label>
                            <div className="form-floating mb-3">
                                <input onChange={handleInputChange} required className="form-control" id="lastName" type="text" name="last_name" placeholder="lastName" aria-describedby="lastName" aria-invalid="false" />
                                <label style={{color: 'rgba(171, 183, 183, 1)'}} htmlFor="lastName">last name...</label>
                            </div>
                            <label style={{fontFamily: 'orbitron'}} htmlFor="email">Email:</label>
                            <div className="form-floating mb-3">
                                <input onChange={handleInputChange} required className="form-control" id="email" type="email" name="email" placeholder="example@carcar.com" aria-describedby="email" aria-invalid="false" />
                                <label style={{color: 'rgba(171, 183, 183, 1)'}} htmlFor="email">example@carcar.com</label>
                            </div>
                            <label style={{fontFamily: 'orbitron'}} htmlFor="password">Password:</label>
                            <div className="form-floating mb-3">
                                <input onChange={handleInputChange} required className="form-control" id="password" type="password" name="password" placeholder="password" aria-describedby="password" aria-invalid="false" />
                                <label style={{color: 'rgba(171, 183, 183, 1)'}} htmlFor="password">password...</label>
                            </div>
                            <label style={{fontFamily: 'orbitron'}} htmlFor="passwordConfirmation">Password Confirmation:</label>
                            <div className="form-floating mb-3">
                                <input onChange={handleInputChange} required className="form-control" id="passwordConfirmation" type="password" name="password_confirmation" placeholder="passwordConfirmation" aria-describedby="passwordConfirmation" aria-invalid="false" />
                                <label style={{color: 'rgba(171, 183, 183, 1)'}} htmlFor="passwordConfirmation">password...</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )

}

export default SignupForm
