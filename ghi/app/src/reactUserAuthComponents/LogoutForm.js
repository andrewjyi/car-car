import React, { useEffect } from 'react'
import { useAuth } from "./AuthProvider"     //AuthProvider is a component aka hooks that we hook


function LogoutForm() {

    const auth = useAuth()

    auth.logOut()

}

export default LogoutForm

//noneed for logoutform so far
