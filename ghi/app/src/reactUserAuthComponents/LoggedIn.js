import React from "react"
import { Navigate, Outlet } from "react-router-dom"
import { useAuth } from "./AuthProvider"     //AuthProvider is a component aka hooks that we hook


const LoggedIn = () => {
    const auth = useAuth()
    if (!auth.token) return <Navigate to="/login/" />
    return <Outlet />
}

export default LoggedIn
