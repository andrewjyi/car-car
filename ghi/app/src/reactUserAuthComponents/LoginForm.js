import { useState } from 'react'
import { useAuth } from './AuthProvider'


function LoginForm() {

    const [formData, setFormData] = useState( { username: '', email: '', password: '' } )

    const auth = useAuth()

    const handleSubmit = (event) => {
        event.preventDefault()
        if (formData.username !== '' && formData.email !== '' && formData.password!== '') {
            auth.loginAction(formData)       //useAuth() to use loginAction
            return
        }
        alert("please provide valid inputs")
    }

    const handleInputChange = (event) => {
        const { name, value } = event.target  // name = event.target.name    value = event.target.value
        setFormData((formData) => ({
            ...formData,
            [name]: value,             // will set form data with email and password inputs as they type it
        }))
    }

    return (
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div style={{backgroundColor: "rgba(210, 238, 130, .5)", boxShadow: '0px 0px 25px 15px rgb(232, 255, 195)'}} className="p-4 mt-4">
                        <h1 style={{fontFamily: 'orbitron'}}>Account Login</h1>
                        <form onSubmit={handleSubmit}>
                            <label style={{fontFamily: 'orbitron'}} htmlFor="username">Username:</label>
                            <div className="form-floating mb-3">
                                <input onChange={handleInputChange} required className="form-control" id="username" type="text" name="username" placeholder="username" aria-describedby="username" aria-invalid="false" />
                                <label style={{color: 'rgba(171, 183, 183, 1)'}} htmlFor="username">username...</label>
                            </div>
                            <label style={{fontFamily: 'orbitron'}} htmlFor="email">Email:</label>
                            <div className="form-floating mb-3">
                                <input onChange={handleInputChange} required className="form-control" id="email" type="email" name="email" placeholder="example@carcar.com" aria-describedby="email" aria-invalid="false" />
                                <label style={{color: 'rgba(171, 183, 183, 1)'}} htmlFor="email">example@carcar.com</label>
                            </div>
                            <label style={{fontFamily: 'orbitron'}} htmlFor="password">Password:</label>
                            <div className="form-floating mb-3">
                                <input onChange={handleInputChange} required className="form-control" id="password" type="password" name="password" placeholder="password" aria-describedby="password" aria-invalid="false" />
                                <label style={{color: 'rgba(171, 183, 183, 1)'}} htmlFor="password">password...</label>
                            </div>
                            <button className="btn btn-primary">Login</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )

}

export default LoginForm
