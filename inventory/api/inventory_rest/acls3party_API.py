import json
import requests
import os

PEXELS_API_KEY = os.environ["PEXELS_API_KEY"]


def get_Model_Manufacturer_Image_URL(manufacturer, model=None):
    headers = {"Authorization": PEXELS_API_KEY}
    if model == None:
        params = {
            "per_page": 1,
            "query": f"{manufacturer} emblem",
        }
    else:
        params = {
            "per_page": 1,
            "query": f"{manufacturer} {model} car",
        }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["landscape"]}
    except (KeyError, IndexError):
        return {"picture_url": None}
