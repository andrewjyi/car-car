from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Technician, Appointment, AutomobileVO
import json
from .encoders import TechnicianEncoder, AppointmentEncoder


@require_http_methods(["GET", "POST"])
def api_list_created_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.filter(status="created").order_by(
            "date_time"
        )
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
        )
    else:
        content = json.loads(request.body)
        newVin = content["vin"]
        automobileVO = AutomobileVO.objects.filter(vin=newVin).filter(sold=True)
        if automobileVO:
            content["vip"] = True
        else:
            content["vip"] = False
        try:
            technician_id = content["technician"]
            technician = Technician.objects.get(id=technician_id)
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid technician id"},
                status=404,
            )
        try:
            appointment = Appointment.objects.create(**content)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Exception:
            response = JsonResponse(
                {"message": "Could not create the appointment, verify and try again"}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET"])
def api_list_all_appointments(request):
    appointments = Appointment.objects.all().order_by("date_time")
    return JsonResponse(
        {"appointments": appointments},
        encoder=AppointmentEncoder,
    )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_appointment(request, appointmentId):
    if request.method == "GET":
        appointment = Appointment.objects.get(id=appointmentId)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Appointment.objects.filter(id=appointmentId).delete()
        return JsonResponse({"deleted": count > 0})
    elif request.method == "PUT":
        content = json.loads(request.body)
        Appointment.objects.filter(id=appointmentId).update(**content)
        appointment = Appointment.objects.get(id=appointmentId)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all().order_by("last_name")
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Exception:
            response = JsonResponse(
                {
                    "message": "Could not create the technician, try different employee id"
                }
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "DELETE"])
def api_technician(request, technicianId):
    if request.method == "GET":
        technician = Technician.objects.get(id=technicianId)
        return JsonResponse(technician, encoder=TechnicianEncoder, safe=False)
    elif request.method == "DELETE":
        count, _ = Technician.objects.filter(id=technicianId).delete()
        return JsonResponse({"deleted": count > 0})
