from django.urls import path
from .api_views import api_signup, api_login_token
from rest_framework.authtoken.views import obtain_auth_token


urlpatterns = [
    path('token/', obtain_auth_token, name='obtain_auth_token'),
    path("login/", api_login_token, name="api_login_token"),
    path("signup/", api_signup, name="api_signup"),
]
