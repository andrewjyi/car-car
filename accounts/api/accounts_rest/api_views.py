from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from .serializers import UserSerializer
from rest_framework.authtoken.models import Token
from .encoders import UserEncoder
from django.views.decorators.csrf import csrf_exempt


@csrf_exempt
@require_http_methods(["POST"])
def api_signup(request):
    content = json.loads(request.body)
    del content["password_confirmation"]
    print("CONTENT:", content)
    user_model_data = UserSerializer(data=content)
    if user_model_data.is_valid():
        try:
            user = user_model_data.save()
            Token.objects.create(user=user)

            return JsonResponse(
                {"message": "Successfully created user account"}
            )
        except Exception:
            response = JsonResponse(
                {"message": "Could not create user account, please verify and try again"}
            )
            response.status_code = 400
            return response
    return JsonResponse(user_model_data.errors, status=400)


@csrf_exempt
@require_http_methods(["POST"])
def api_login_token(request):
    content = json.loads(request.body)
    print("CONTENT:", content)

    username = content["username"]
    email = content["email"]
    password = content["password"]
    print("PASSWORD:", password)
    user = authenticate(
        request,
        username=username,
        email=email,
        password=password,
    )
    print("USER:", user)
    if user is not None:  #that means user is authenticated and exists in database since matching User object returned

        try:
            token = Token.objects.get(user_id=user.id)
        except Token.DoesNotExist:
            Token.objects.create(user=user)
            token = Token.objects.get(user_id=user.id)
        print("token:", token)
        print("token key:", token.key)
        print("username:", user.username)
        print("email:", user.email)
        userData = {"username": user.username, "email": user.email, "first_name": user.first_name, "last_name": user.last_name, "token": token.key}
        print("USERDATA:", userData)
        return JsonResponse({"user": userData})
