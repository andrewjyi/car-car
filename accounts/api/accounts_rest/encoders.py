from django.conf import settings
from common.json import ModelEncoder


class UserEncoder(ModelEncoder):
    model = settings.AUTH_USER_MODEL,
    properties = [
        "username",
        "email",
        "first_name",
        "last_name",
    ]

