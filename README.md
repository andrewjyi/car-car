# CarCar

Project title – CarCar

![Homepage Image](/images/carCarApp.PNG)

<details>
<summary>Table of Contents</summary>

[TOC]

</details>

---

Team:

* Person 1 -  Andrew Yi – Service Microservice
* Person 2 -  Jahaira Aponte- Sales Microservice


## Diagram DOMAIN DRIVEN DESIGN!

![Diagram of CarCar App Design](/images/CarCarDiagram.png)

## Project Description and Design

Welcome to the CarCar Web Application, a scalable web application that allows car dealerships of all sizes to manage their automobile inventories, their automobile sales, and their automobile service appointments! This scalable web application provides everything a car dealership needs in a user friendly single page application!

As shown in the diagram above, the CarCar Web Application consists of three core back-end microservices:
1. Inventory Micro-Service: contains the Automobile, Model, and Manufacturer Models that we can use to create and list the automobiles available for sale at the dealership.
2. Sales Micro-Service: contains the Sale, SalesPerson, Customer, and AutomobileVO Models that we can use to record and list the automobile sales transactions.
3. Service Micro-Service: contains the Appointment, Technician, and AutomobileVO Models that we can use to create and list automobile service appointments.

The Inventory, Sales, and Service MicroServices all run on internal port 8000 for polling data.
    **Both the Sales and Service MicroServices have integrated polling to request data from the Inventory MicroService for the Automobiles data to create or update AutomobileVOs that are referenced via foreign keys when recording sales. When creating service-appointments, the vin attribute of the appointment will reference the automobileVOs to see if the automobile scheduled for service was sold from their dealership indicating VIP special service**

The Inventory microservice runs on external port 8100 for Restful API data transfer

The Sales microservice runs on external port 8090 for Restful API data transfer

The Service microservice runs on external port 8080 for Restful API data transfer

***API DOCUMENTATION FOR ALL MICROSERVICES BELOW***

The CarCar React Front End Single Page Application runs on port 3000 and can be accessed in a browser using the following URL once installation instructions are followed and the app is running:   **http://localhost:3000/**

***Please take a look through this README documentation before starting***

## GETTING STARTED

Technologies used:
-Docker -React/JavaScript -Django/Python -Gitlab -Git -Node.js

Follow the following instructions in order to download the project in its entirety and run the application. Following the installation instructions will download the application code and all requirements including volumes to point to our appropriate code directories for data to persist while developing, network for internal and external communication via polling, RESTful API, URL pathing, and database configuration, and necessary microservices environments via Docker containers:

## HOW TO RUN THIS APP

Installation instructions and requirements
1. Fork the repository at https://gitlab.com/Jahairaaaa/project-beta
2. Clone the repository to your local computer
3. Change the working directory to the cloned project
4. Build the database, Docker images, and run Docker containers using the following commands in the terminal for your project working directory:
    1. docker volume create beta-data
    2. docker-compose build
    3. docker-compose up

    Note: For macOS users, you can safely ignore any warnings about an environment variable named OS being missing.
    Note: For windowsOS users, please be patient as the react front end microservice may take up to 15 minutes to start.

***ENSURE ALL OF YOUR DOCKER CONTAINERS ARE RUNNING***

## DOCUMENTATION

### Service microservice

The Service microservice allows the dealership to add service technician employees into the system and record scheduled service appointments assigned to specific technicians. The web application allows us to view a list of the technicians employed, a list of the service appointments scheduled with the ability to cancel or finish them, and also a list of all of the service appointments history that can be filtered by VIN (vehicle identification number).

The Service microservice consists of three models: Appointment, Technician, and AutomobileVO

Appointment:
    The Appointment model has attributes: date_time, reason, status, vin, customer, and technician which is a foreign key to the Technician model allowing us to assign a technician to a service appointment.

Technician:
    The Technician model has attributes: first_name, last_name, and employee_id which must be a unique string of digits and characters per technician

#### Value Object for Service MicroService: AutomobileVO:
    The AutomobileVO model has attributes: vin and sold. The AutomobileVOs are created by polling the Inventory microService for its Automobiles data. The AutomobileVOs are references of the Automobiles in the Inventory microService. For the Service microService, the AutomobileVOs are used to see if Appointment VINs match the VINs of AutomobileVOs in order to designate the Service appointment customer as a VIP for special service.

### Sales microservice

Sales Microservice – Sales microservice helps add customers, sales, and salespeople
while also tracking a list of salespersonnel, customers, and sales. It uses four models:
Salesperson, AutomobileVO, Customer, and Sale. The Sale model is dependent on the
Salesperson, Customer, and AutomobileVO because it needs that information to record a sale.
The web application also allows us to view a list of all sales that can be filtered by salesPerson to see the sales of each salesPerson employee.

Sale:
    The Sale model has attributes: automobile, sales_person, customer, and price. automobile, sales_person, and customers are all foreign keys to the AutomobileVO, SalesPerson, and Customer models respectively allowing us to assign a automobile, salesPerson, and customer to the sale along with the price the vehicle sold for.

Customer:
    The Customer model has attributes: first_name, last_name, address, and phone_number.

SalesPerson:
    The SalesPerson model has attributes: first_name, last_name, and employee_id which must be a unique number per salesPerson.

#### Value Object for Sales MicroService: AutomobileVO:
    The AutomobileVO model has attributes: vin and sold. Just like the AutomobileVOs in the Service microService, the AutomobileVOs for the Sales microService are also created by polling the Inventory microService for its Automobiles data. The AutomobileVOs are references of the Automobiles in the Inventory microService. For the Sales microService, the AutomobileVOs are a Sale Model foreign key. This is to select the Automobile being sold when recording a sale.

## API (CRUD ROUTE) DOCUMENTATION: RESTful API with URLs and Ports

![Diagram of CarCar API](/images/CarCarAPI.png)

***Get(View), Post(Create), Put(Update), and Delete Data via Internet Browser & Insomnia:  use the following API URLs and Instructions***

## Service MicroService (PORT 8080):

### APPOINTMENTS: replace **':id'** with the id number of a specific appointment

| Action | Method |	URL
| ----------- | ----------- | ----------- |
| List appointments | GET |	http://localhost:8080/api/appointments/
| **Create an appointment | POST | http://localhost:8080/api/appointments/
| Delete an appointment | DELETE | http://localhost:8080/api/appointments/:id/
| **Set appointment status to "canceled" | PUT | http://localhost:8080/api/appointments/:id/cancel/
| **Set appointment status to "finished" | PUT | http://localhost:8080/api/appointments/:id/finish/

#### Creating and Updating an appointment requires appointment details sent as JSON body of the POST or PUT request

***replace example JSON body with your appointment details: for example, replace "Windshield Repair" with "your specific reason for the service appointment"***
***updates only require attributes you are updating: for example, while creating requires the format below, updating you can leave jsut the line "reason": "door repair" in the JSON body and it will update the existing appointment details to reflect the change in reason***

JSON body format:

{
	"date_time": "2024-02-23:05:30",
	"reason": "Windshield Repair",
	"status": "created",
	"vin": "ZFF74UFA3E0196762",
	"customer": "Andrew Yi",
	"technician": 2
}

***status above must be "created", "cancel", or "finish"***
***the number "2" above for technician references the technician 'id' of the technician being assigned to the service appointment. to get the id number for technicians, use the get request for list technicians below***

### TECHNICIANS: replace **':id'** with the id number of a specific technician

| Action | Method |	URL
| ----------- | ----------- | ----------- |
| List technicians | GET | http://localhost:8080/api/technicians/
| **Create a technician | POST | http://localhost:8080/api/technicians/
| Delete a specific technician | DELETE | http://localhost:8080/api/technicians/:id/

#### Creating a technician requires technician details sent as JSON body of the POST request

***replace example JSON body with your technician details***

JSON body format:

{
	"first_name": "John",
	"last_name": "Doe",
	"employee_id": "jdoe1"
}

***employee_id for technician must be a unique per technician***

## Sales MicroService (PORT 8090):

### SALES endpoints: replace **'id'** with the id number of a specific sale

| Action | Method | URL
| ----------- | ----------- | ----------- |
|List sales | GET | http://localhost:8090/api/sales/
|Create a sale | POST | http://localhost:8090/api/sales/
|Delete a sale | DELETE | http://localhost:8090/api/sales/:id

#### Creating a sale requires sale details sent as JSON body of the POST request

***replace example JSON body with your sale details***

JSON body format:

{
    "automobile": "1C3CC5FB2AN120326",
    "sales_person": 2,
    "customer": 3,
    "price": 60
}

***automobile entry must be the automobile VIN of the automobile being sold and it must be an automobile in the inventory of Automobiles***
***the number "2" above for sales_person references the salesPerson 'id' of the salesPerson who handled this sale. to get the id number for salesPersons, use the get request for list salespeople below***
***the number "3" above for customer references the customer 'id' of the customer who bought the automobile. to get the id number for customers, use the get request for list customers below***

### SALESPERSONS endpoints: replace **'id'** with the id number of a specific salesperson

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List salespeople | GET | http://localhost:8090/api/salespeople/
| Create a salesperson | POST | http://localhost:8090/api/salespeople/
| Delete salesperson | DELETE | http://localhost:8090/api/salespeople/id/

#### Creating a salesperson requires salesperson details sent as JSON body of the POST request

***replace example JSON body with your salesperson details***

JSON body format:

{
	"first_name": "John",
	"last_name": "Doe",
	"employee_id": "111"
}

***employee_id for salesperson must be a unique number per salesperson***

### CUSTOMERS endpoints: replace **'id'** with the id number of a specific customer

| Action | Method | URL
| ----------- | ----------- | ----------- |
|List customers | GET | http://localhost:8090/api/customers/
|Create a customer | POST | http://localhost:8090/api/customers/
|Delete a customer | DELETE | http://localhost:8090/api/customers/id/

#### Creating a customer requires customer details sent as JSON body of the POST request

***replace example JSON body with your customer details***

JSON body format:

{
	"first_name": "John",
	"last_name": "Doe",
	"address": "123 Example Ln. City, ST 12345",
	"phone_number": 1234567890
}

## Inventory MicroService (PORT 8100):

### AUTOMOBILES: replace **':vin'** with the vin of a specific automobile

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List automobiles | GET | http://localhost:8100/api/automobiles/
| Create an automobile | POST | http://localhost:8100/api/automobiles/
| Get a specific automobile | GET | http://localhost:8100/api/automobiles/:vin/
| Update a specific automobile | PUT | http://localhost:8100/api/automobiles/:vin/
| Delete a specific automobile | DELETE | http://localhost:8100/api/automobiles/:vin/

#### Creating and updating an automobile requires automobile details sent as JSON body of the POST request.

***ONLY COLOR, YEAR AND SOLD CAN BE UPDATED***

***replace example JSON body with your automobile details***

JSON body format: POST

{
  "color": "red",
  "year": 2012,
  "vin": "1C3CC5FB2AN120174",
  "model_id": 1
}

***for the vin, enter the VIN of the automobile available for sale***
***the number "1" above for model_id references the VehicleModel 'id'. To get the id number for VehicleModels, use the get request for list vehicle models below***

JSON body format: PUT

{
  "color": "red",
  "year": 2012,
  "sold": true
}

***for put request, use 1 to 3 of the above attributes to update. sold is a boolean, must be true or false***

### VEHICLEMODELS: replace **':id'** with the id number of a specific customer

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List vehicle models | GET | http://localhost:8100/api/models/
| Create a vehicle model | POST | http://localhost:8100/api/models/
| Get a specific vehicle model | GET | http://localhost:8100/api/models/:id/
| Update a specific vehicle model | PUT | http://localhost:8100/api/models/:id/
| Delete a specific vehicle model | DELETE | http://localhost:8100/api/models/:id/

#### Creating and updating a vehicleModel requires vehicleModel details sent as JSON body of the POST request.

***ONLY NAME AND PICTURE_URL CAN BE UPDATED***

***replace example JSON body with your vehicleModel details***

JSON body format: POST

{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer_id": 1
}

***for the picture_url, enter the URL of the vehicleModel picture***
***the number "1" above for manufacturer_id references the Manufacturer 'id'. To get the id number for Manufacturers, use the get request for list manufacturers below***

JSON body format: PUT

{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg"
}

***for put request, use 1 to 2 of the above attributes to update.***

### MANUFACTURER: replace **':id'** with the id number of a specific customer

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List manufacturers | GET | http://localhost:8100/api/manufacturers/
| Create a manufacturer | POST | http://localhost:8100/api/manufacturers/
| Get a specific manufacturer | GET | http://localhost:8100/api/manufacturers/:id/
| Update a specific manufacturer | PUT | http://localhost:8100/api/manufacturers/:id/
| Delete a specific manufacturer | DELETE | http://localhost:8100/api/manufacturers/:id/

#### Creating and updating a Manufacturer requires Manufacturer details sent as JSON body of the POST request.

***replace example JSON body with your Manufacturer details***

JSON body format:

{
  "name": "Chrysler"
}

## REACT FRONT END SINGLE PAGE APPLICATION (Port 3000) *** USE INTERNET BROWSER AT http://localhost:3000/ ***

Now that you have seen how the data is managed via the DJANGO PYTHON BACK END MICRO SERVICES SERVERS!

It is time to interact with the SINGLE PAGE APPLICATION to VIEW and MANAGE/ORGANIZE your CAR DEALERSHIP SERVICES, SALES, and INVENTORY all through ONE PAGE created via REACT JAVASCRIPT!

HOME PAGE:

![Homepage Image](/images/carCarApp.PNG)

![Manufacturers List page Image](/images/manufacturersList.PNG)

![Create Manufacturer page Image](/images/manufacturerForm.PNG)

![models List page Image](/images/modelsList.PNG)

![Create model page Image](/images/modelForm.PNG)

![automobiles List page Image](/images/automobilesList.PNG)

![Create automobile page Image](/images/automobileForm.PNG)

#CHECK THE REST OF THE PAGES OUT YOURSELF!!

19 REACT COMPONENTS FILLED WITH INTERACTIVE FEATURES!!

Browser URL: http://localhost:3000/

*** User Friendly Website! Easy navigation! Explore and organize your Car Dealership and Track the Best Selling Cars! the Best Selling SalesPersons! the Most Productive Technicians! and COUNT THE MONEY FLOWING IN!!! ***
